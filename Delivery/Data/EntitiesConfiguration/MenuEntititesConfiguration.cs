﻿using Delivery.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Delivery.Data.EntitiesConfiguration
{
    internal class MenuEntititesConfiguration : IEntityTypeConfiguration<Menu>
    {
        public void Configure(EntityTypeBuilder<Menu> builder)
        {
           
            builder.HasOne<Dish>()
                   .WithMany()
                   .HasForeignKey(x => x.DishId)
                   .IsRequired();

            builder.HasOne<Restaurant>()
                   .WithMany()
                   .HasForeignKey(x => x.RestaurantId)
                   .IsRequired();

        }
    }
}
