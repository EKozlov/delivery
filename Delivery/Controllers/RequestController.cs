﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Delivery.Data;
using Delivery.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Delivery.Controllers
{
    public class RequestController : Controller
    {
        private readonly ApplicationDbContext _db;
        public RequestController(ApplicationDbContext db)
        {
            _db = db;
        }


        public async Task<IActionResult> CreateRequestAsync(Request request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                _db.Requests.Add(new Entities.Request
                {
                    Name = request.Name,
                    Dish = new Dish
                    {
                        Cost = request.Dish.Cost,

                        Description = request.Dish.Description
                    }
                });

                transaction.Commit();
            }

            return View();
        }

        public IActionResult Index()
        {
            var result = _db.Requests.ToList();

            return View(result);
        }
    }
}