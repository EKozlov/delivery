﻿using Delivery.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Delivery.Data.EntitiesConfiguration
{
    public class RestaurantEntittiesConfiguration : IEntityTypeConfiguration<Restaurant>
    {
        public void Configure(EntityTypeBuilder<Restaurant> builder)
        {

            builder.HasOne<Menu>()
                   .WithMany()
                   .HasForeignKey(x => x.MenuId)
                   .IsRequired(false);

            
        }
    
    }
}
