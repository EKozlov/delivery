﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Delivery.Entities
{
    public class Request
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Dish Dish { get; set; }

    }
}
