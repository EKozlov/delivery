﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Delivery.Data;
using Delivery.Entities;
using Microsoft.AspNetCore.Mvc;

namespace Delivery.Controllers
{
    public class RestaurantController : Controller
    {
        private readonly ApplicationDbContext _db;
        public RestaurantController(ApplicationDbContext db)
        {
            _db = db;
        }
        public IActionResult Index()
        {
            var restaurants = _db.Restaurants.ToList();

            return View(restaurants);
        }

        
        public IActionResult Create(string Name)
        {
            _db.Restaurants.Add(new Restaurant { Name = Name });

            _db.SaveChanges();

            return View();
        }

       
        public IActionResult Put(int id, Restaurant model)
        {
            var restaurant = _db.Restaurants.Where(x => x.Id == id).FirstOrDefault();
            restaurant.Name = model.Name;
            _db.SaveChanges();
            return View();
        }

        
        public IActionResult GetById(int id)
        {
            var restaurant = _db.Restaurants.Where(x => x.Id == id).FirstOrDefault();

            return View(restaurant);
        }
        
        public IActionResult Delete(int id)
        {
            var restaurant = _db.Restaurants.Where(x => x.Id == id).FirstOrDefault();

            _db.Restaurants.Remove(restaurant);

            return View();
        }

    }
}