﻿using System;
using System.Collections.Generic;
using System.Text;
using Delivery.Data.EntitiesConfiguration;
using Delivery.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Delivery.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<Dish> Dishes { get; set; }
        public DbSet<Restaurant> Restaurants { get; set; }

        public DbSet<Request> Requests { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            
            modelBuilder.ApplyConfiguration(new MenuEntititesConfiguration());
            modelBuilder.ApplyConfiguration(new RestaurantEntittiesConfiguration());
            modelBuilder.ApplyConfiguration(new RequestEntitiesConfiguration());
        }



    }
}
