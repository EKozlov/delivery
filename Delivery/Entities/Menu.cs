﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Delivery.Entities
{
    public class Menu
    {
        public int Id { get; set; }
        public int DishId { get;set;}

        public int RestaurantId { get; set; }
    }
}
